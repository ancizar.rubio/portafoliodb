CREATE TABLE usuarios (
idUsuario INT UNSIGNED AUTO_INCREMENT,
nombreUsuario VARCHAR(30) NOT NULL,
telefonoUsuario VARCHAR(30) NOT NULL,
correoUsuario VARCHAR(50),
compañiaUsuario VARCHAR(50),
comentarioUsuario VARCHAR(150),
PRIMARY KEY(idUsuario),
INDEX(idUsuario)
);

CREATE TABLE asunto(
idAsunto INT UNSIGNED AUTO_INCREMENT,
nombreAsunto VARCHAR(30) NOT NULL,
PRIMARY KEY(idAsunto),
INDEX(idAsunto)
);

ALTER TABLE usuarios 
ADD CONSTRAINT FK_idasunto
FOREIGN KEY (idAsunto) 
REFERENCES asunto (idAsunto) 
ON UPDATE CASCADE ON DELETE RESTRICT;

CREATE TABLE `asunto` (
  `idAsunto` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nombreAsunto` VARCHAR(30) NOT NULL,
  PRIMARY KEY (`idAsunto`),
  INDEX `idAsunto` (`idAsunto` ASC) VISIBLE)
ENGINE = InnoDB;



-- -----------------------------------------------------
-- Table `portafolio`.`usuarios`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `portafolio`.`usuarios` (
  `idUsuario` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nombreUsuario` VARCHAR(30) NOT NULL,
  `telefonoUsuario` VARCHAR(30) NOT NULL,
  `correoUsuario` VARCHAR(50) NULL DEFAULT NULL,
  `compañiaUsuario` VARCHAR(50) NULL DEFAULT NULL,
  `comentarioUsuario` VARCHAR(150) NULL DEFAULT NULL,
  `asunto_idAsunto` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`idUsuario`, `asunto_idAsunto`),
  INDEX `idUsuario` (`idUsuario` ASC) VISIBLE,
  INDEX `fk_usuarios_asunto_idx` (`asunto_idAsunto` ASC) VISIBLE,
  CONSTRAINT `fk_usuarios_asunto`
    FOREIGN KEY (`asunto_idAsunto`)
    REFERENCES `portafolio`.`asunto` (`idAsunto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
    ENGINE = InnoDB;
    
INSERT INTO asunto
       VALUES ('1','contratacion');

INSERT INTO asunto
       VALUES ('2','prestacion de servicios');

INSERT INTO asunto
       VALUES ('3','contacto');